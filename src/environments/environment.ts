// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCrjdDWrHaVHIbct-k1IRwivXuAVXzBsqU',
    authDomain: 'agile-01.firebaseapp.com',
    databaseURL: 'https://agile-01.firebaseio.com',
    projectId: 'agile-01',
    storageBucket: 'agile-01.appspot.com',
    messagingSenderId: '64896060699',
    appId: '1:64896060699:web:a78cefe9d2f9ac4c8517be'
  }
};
