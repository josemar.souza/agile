import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationService } from 'app/services/notification.service';
import { TipoNotificacao } from 'app/models/tipo-notificacao.enum';
import { Usuario } from 'app/models/usuario.model';
import { Perfil } from 'app/models/perfil.enum';
import { AutenticacaoService } from 'app/services/autenticacao.service';
import { UsuarioService } from 'app/services/usuario.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-atendente',
  templateUrl: './atendente.component.html'
})
export class AtendenteComponent implements OnInit {
  public atendentes: Observable<Usuario[]>;
  public exibeNovo = false;
  public formAtendente: FormGroup;
  private subjectPesquisa: Subject<string> = new Subject<string>();
  constructor(private notificacao: NotificationService,
              private autenticaoService: AutenticacaoService,
              private usuarioService: UsuarioService,
              private loadingBar: LoadingBarService,
              private firestore: AngularFirestore
              ) { }

  ngOnInit() {
    this.atendentes = this.usuarioService.listarPorClientePerfil(this.autenticaoService.cliente.id, Perfil.ATENDENTE);
    this.exibeNovo = false;
    this.initForm(new Usuario('', '', ''));
  }
  initForm(atendente: Usuario) {
    this.formAtendente = new FormGroup({
      id: new FormControl(atendente.id),
      nome: new FormControl(atendente.nome, [Validators.required, Validators.minLength(10), Validators.pattern('^[a-zA-Z ]*$')]),
      email: new FormControl(atendente.email, [Validators.required, Validators.email]),
      login: new FormControl(atendente.login, [Validators.required, Validators.pattern('^[.0-9|a-z]*$')]),
      usuarioId: new FormControl(atendente.id)
    }, { updateOn: 'blur' });
  }
  async onSubmit() {
    this.loadingBar.start();
    const _self = this;
    try {
      if (!this.formAtendente.valid) {
        throw new Error('Informe todos os campos obrigatórios');
      }
      const usuarioAtendente: Usuario = new Usuario(this.formAtendente.controls['nome'].value, '', '');
      usuarioAtendente.email = this.formAtendente.controls['email'].value;
      usuarioAtendente.id = this.formAtendente.controls['usuarioId'].value;
      usuarioAtendente.login = this.formAtendente.controls['login'].value;
      usuarioAtendente.clienteId = this.autenticaoService.cliente.id;
      usuarioAtendente.perfis = new Array<Perfil>();
      usuarioAtendente.perfis.push(Perfil.ATENDENTE);
      await this.usuarioService.salvar(usuarioAtendente)
        .then(async function() {
          _self.ngOnInit();
          _self.notificacao.showNotification(TipoNotificacao.SUCCESS, 'Salvo com sucesso!');
          //TODO: Enviar email para o novo usuário para criar a senha
          /*
          usuarioAtendente.senha = '';
          await _self.autenticaoService.addUser(usuarioAtendente).then(function(userCredential) {
            usuarioAtendente.uid = userCredential.user.uid;
            _self.usuarioService.salvar(usuarioAtendente);
            _self.ngOnInit();
            _self.notificacao.showNotification(TipoNotificacao.SUCCESS, 'Salvo com sucesso!');
          }).catch(function(error) {
            throw error;
          })
          */
        })
        .catch(function(error) {
          throw error;
        });
    } catch (error) {
      if (error.name === 'validation-error') {
        this.notificacao.showNotification(TipoNotificacao.WARNING, error.message);
      } else {
        this.notificacao.showNotification(TipoNotificacao.ERROR, error.message);
      }
    } finally {
      this.loadingBar.stop();
    }
  }
  editar(item: Usuario) {
    this.initForm(item);
    this.exibeNovo = true;
  }
  alternar() {
    this.exibeNovo = !this.exibeNovo;
  }
  onBlurNome() {
    if (this.formAtendente.controls['nome'].value.length > 0) {
      const nomeArray: string[] = this.formAtendente.controls['nome'].value.split(' ', 10);
      if (nomeArray.length > 0) {
        this.formAtendente.controls['login'].setValue(nomeArray[0].toLowerCase() + '.' + nomeArray[nomeArray.length - 1].toLowerCase());
      }
    }
  }
  onChangeBusca(termo: string) {
    this.ngOnInit();
    if (termo) {
      this.atendentes =
        this.atendentes.pipe(map(usuarios => usuarios.filter(user => user.nome.toLowerCase().startsWith(termo.toLowerCase()))));
    }
  }
}
