import { Component, OnInit } from '@angular/core';
import { Cliente } from 'app/models/cliente.model';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationService } from 'app/services/notification.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { Plano } from 'app/models/plano.model';
import { TipoNotificacao } from 'app/models/tipo-notificacao.enum';
import { ClienteService } from 'app/services/cliente.service';
import { PlanoService } from 'app/services/plano.service';
import { map, first } from 'rxjs/operators';
import { TipoCliente } from 'app/models/tipo-cliente.enum';
import { Contato } from 'app/models/contato.model';
import { TipoFaturamento } from 'app/models/tipo-faturamento.enum';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {
  public clientes: Observable<Cliente[]>;
  public planos: Observable<Plano[]>;
  public exibeNovo = false;
  public formCliente: FormGroup;
  private clienteId: string = null;
  public plano: Plano = new Plano('', '', null, 0);
  public tiposFaturamento = Object.keys(TipoFaturamento).map(key => TipoFaturamento[key]);
  constructor(private notificacao: NotificationService,
              private clienteService: ClienteService,
              private planoService: PlanoService,
              private loadingBar: LoadingBarService) { }
  ngOnInit() {
    this.clientes = this.clienteService.list();
    this.planos = this.planoService.listarOrdenadoPorValor();
    this.exibeNovo = false;
    this.clienteId = null;
    this.plano = new Plano('', '', TipoFaturamento.MENSAL, 29.9);
    //this.plano.id = 'vCx6UkLbwLheQoBhd4Xh';
    //this.plano.vencimento = new Date();
    //this.plano.vencimento.setDate(this.plano.vencimento.getDate() + 30);
    this.initForm(new Cliente(null, '', '', this.plano));
  }
  initForm(cliente: Cliente) {
    this.formCliente = new FormGroup({
      id: new FormControl(cliente.id),
      nome: new FormControl(cliente.nome, [Validators.required, Validators.minLength(10)]),
      documento: new FormControl(cliente.documento, Validators.required),
      contatoTelefone: new FormControl(cliente.contato.telefone),
      contatoCelular: new FormControl(cliente.contato.celular),
      contatoEmail: new FormControl(cliente.contato.email, [Validators.required, Validators.email]),
      plano: new FormControl(cliente.plano, Validators.required),
      tipoFaturamento: new FormControl(cliente.plano.tipoFaturamento, Validators.required),
      vencimento: new FormControl(cliente.plano.vencimento, Validators.required),
      valor: new FormControl(cliente.plano.valor, Validators.required)
    }, { updateOn: 'blur' });
  }
  async onSubmit() {
    this.loadingBar.start();
    const _self = this;
    try {
      if (this.formCliente.valid) {
        if (this.plano.id) {
          const documento = this.formCliente.get('documento').value;
          const nome = this.formCliente.get('nome').value;
          const contatoTelefone = this.formCliente.get('contatoTelefone').value;
          const contatoCelular = this.formCliente.get('contatoCelular').value;
          const contatoEmail = this.formCliente.get('contatoEmail').value;
          const tipoCliente: TipoCliente  = this.formCliente.get('documento').value.length > 15 ?
                                              TipoCliente.PESSOA_JURIDICA : TipoCliente.PESSOA_FISICA;
          const planoSelecionado = this.plano;
          planoSelecionado.valor = this.formCliente.get('valor').value;
          planoSelecionado.vencimento = new Date(this.formCliente.get('vencimento').value);
          planoSelecionado.tipoFaturamento = this.formCliente.get('tipoFaturamento').value;
          const cliente: Cliente = new Cliente(tipoCliente, documento, nome, planoSelecionado);
          cliente.contato = new Contato(contatoCelular, contatoTelefone, contatoEmail);
          cliente.id = this.clienteId;
          await this.clienteService.salvar(cliente).then(async function() {
            _self.notificacao.showNotification(TipoNotificacao.SUCCESS, 'Salvo com sucesso!');
            _self.ngOnInit();
          })
          .catch(function(error) {
            throw error;
          });
        } else {
          this.notificacao.showNotification(TipoNotificacao.WARNING, 'Selecione um plano');
        }
      } else {
        this.notificacao.showNotification(TipoNotificacao.WARNING, 'Informe todos os campos obrigatórios');
      }
    } catch (error) {
      if (error.name === 'validation-error') {
        this.notificacao.showNotification(TipoNotificacao.WARNING, error.message);
      } else {
        this.notificacao.showNotification(TipoNotificacao.ERROR, error.message);
      }
    } finally {
      this.loadingBar.stop();
    }
  }
  editar(item: Cliente) {
    this.initForm(item);
    this.exibeNovo = true;
    this.clienteId = item.id;
    this.plano = item.plano;
  }
  alternar() {
    this.exibeNovo = !this.exibeNovo;
  }
  onChangeBusca(termo: string) {
    this.ngOnInit();
    if (termo) {
      this.clientes =
        this.clientes.pipe(map(clientes => clientes.filter(cliente => cliente.nome.toLowerCase().startsWith(termo.toLowerCase()))));
    }
  }
  selecionarPlano(item: Plano) {
    this.plano = item;
    this.plano.tipoFaturamento = TipoFaturamento.MENSAL;
    const vencimento = new Date();
    vencimento.setDate(vencimento.getDate() + 30);
    this.plano.vencimento = vencimento;
    this.formCliente.get('plano').setValue(this.plano);
    if (!this.clienteId) {
      this.formCliente.get('vencimento').setValue(this.plano.vencimento);
    }
    this.formCliente.get('tipoFaturamento').setValue(this.plano.tipoFaturamento);
    this.formCliente.get('valor').setValue(this.plano.valor);
  }
  onChangeTipoFaturamento(tipoFaturamento: TipoFaturamento) {
    let valor = this.plano.valor;
    switch (tipoFaturamento) {
      case TipoFaturamento.TRIMESTRAL: {
        valor = valor * 3 * 0.9;
        break;
      }
      case TipoFaturamento.ANUAL: {
        valor = valor * 12 * 0.75;
        break;
      }
    }
    this.formCliente.get('valor').setValue(valor);
  }
}
