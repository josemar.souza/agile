import { Cliente } from 'app/models/cliente.model';
import { Usuario } from 'app/models/usuario.model';
import { Perfil } from 'app/models/perfil.enum';
import { Injectable } from '@angular/core';
import { UsuarioService } from './usuario.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseApp } from '@angular/fire';
import { TipoCliente } from 'app/models/tipo-cliente.enum';

@Injectable({providedIn: 'root'})
export class AutenticacaoService {
    public cliente: Cliente;
    public usuario: Usuario;
    constructor(private angularFireAuth: AngularFireAuth, private usuarioService: UsuarioService) {
        this.initMokup();
        /*
        this.angularFireAuth.authState.subscribe(userResponse => {
            if (userResponse) {
              localStorage.setItem('user', JSON.stringify(userResponse));
            } else {
              localStorage.setItem('user', null);
            }
          });*/
    }
    public async addUser(usuario: Usuario): Promise<any> {
        return this.angularFireAuth.auth.createUserWithEmailAndPassword(usuario.email, usuario.senha);
    }
    public initMokup(): void {
        const perfis = new Array<Perfil>();
        perfis.push(Perfil.ADMINISTRADOR);
        this.usuario = new Usuario('Josemar M Souza', 'josemar.souza', 'josemar.souza@live.com');
        this.usuario.email = 'josemar.souza@live.com';
        this.usuario.login = 'josemar.souza';
        this.usuario.perfis = perfis;
        this.usuario.id = 'TkFbKNEHxUaGG7ahYpFa';
        this.cliente = new Cliente(TipoCliente.PESSOA_JURIDICA, '', 'Empresa Teste', null);
        this.cliente.id = 'wWGd3iixqJp27EPwcMU5';
    }
}