import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { TipoNotificacao } from 'app/models/tipo-notificacao.enum';

@Injectable({providedIn: 'root'})
export class NotificationService {
    constructor(private toastr: ToastrService) { }
    showNotification(tipo: TipoNotificacao, mensagem: string) {
        const mensagemHtml: string =    '<span data-notify="icon" class="#icon text-dark"></span>' +
                                        '<span data-notify="message" class="text-dark">' +
                                            mensagem +
                                        '</span>';
        switch (tipo) {
          case TipoNotificacao.INFO:
            this.toastr.info(mensagemHtml.replace('#icon', 'nc-icon nc-bell-55'), '',
              {
                timeOut: 4000,
                closeButton: true,
                enableHtml: true,
                toastClass: 'alert alert-info alert-with-icon',
                positionClass: 'toast-top-right'
              }
            );
            break;
          case TipoNotificacao.SUCCESS:
            this.toastr.success(
                mensagemHtml.replace('#icon', 'nc-icon nc-bell-55'),
                '',
                {
                    timeOut: 4000,
                    closeButton: true,
                    enableHtml: true,
                    toastClass: 'alert alert-success alert-with-icon',
                    positionClass: 'toast-top-right'
              }
            );
            break;
          case TipoNotificacao.WARNING:
            this.toastr.warning(
                mensagemHtml.replace('#icon', 'nc-icon nc-bell-55'),
                '',
                {
                    timeOut: 4000,
                    closeButton: true,
                    enableHtml: true,
                    toastClass: 'alert alert-warning alert-with-icon',
                    positionClass: 'toast-top-right'
                }
            );
            break;
          case TipoNotificacao.ERROR:
            this.toastr.error(
                mensagemHtml.replace('#icon', 'nc-icon nc-bell-55'),
                '',
                {
                    timeOut: 4000,
                    enableHtml: true,
                    closeButton: true,
                    toastClass: 'alert alert-danger alert-with-icon',
                    positionClass: 'toast-top-right'
                }
            );
            break;
          case TipoNotificacao.DEFAULT:
            this.toastr.show(
                mensagemHtml.replace('#icon', 'nc-icon nc-bell-55'),
                '',
                {
                    timeOut: 4000,
                    closeButton: true,
                    enableHtml: true,
                    toastClass: 'alert alert-primary alert-with-icon',
                    positionClass: 'toast-top-right'
                }
            );
            break;
          default:
            break;
        }
      }
}
