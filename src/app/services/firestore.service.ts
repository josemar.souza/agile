import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, DocumentChange } from '@angular/fire/firestore';
import { Model } from 'app/models/model';

export class FirestoreService<T extends Model> {
    protected firestoreCollection: AngularFirestoreCollection<T>;
    private _firestore: AngularFirestore;
    constructor(firestore: AngularFirestore, collectionName: string) {
        this._firestore = firestore;
        this.firestoreCollection = firestore.collection<T>(collectionName);
    }
    public find(id: string): Observable<any> {
        try {
            return this.firestoreCollection.doc(id).valueChanges();
        } catch (error) {
            throw new Error('Falha ao buscar ' + this.constructor.name);
        }
    }
    /*
    public async getReferenceBy(attributes: string[], values: string[]): DocumentChange[] {
        const collectionReference = this.firestoreCollection.ref;
        const query = collectionReference.where(attributes[0], '==', values[0]).limit(1);
        let documentChange: DocumentChange[];
        return query.get().then(function(querySnapshot) {
            documentChange = querySnapshot.docChanges();
        });*/
/*
        return this._firestore.collection<T>(this.collectionName, ref =>
            ref.where(attributes[0], '==', values[0]).limit(1)
        ).valueChanges();
    }*/
    public list(): Observable<T[]> {
        try {
            return this.firestoreCollection.valueChanges();
        } catch (error) {
            throw new Error('Falha ao listar ' + this.constructor.name);
        }
    }
    public async add(entity: T): Promise<void> {
        try {
            const id = this._firestore.createId();
            entity.id = id;
            return await this.firestoreCollection
                    .doc(id)
                    .set(JSON.parse(JSON.stringify(entity)))
                    .catch(function(error) {
                        throw new Error(error.message);
                    });
        } catch (error) {
            throw new Error('Falha ao adicionar ' + entity.constructor.name);
        }
    }
    public async update(entity: T): Promise<void> {
        try {
            return this.firestoreCollection.doc(entity.id).update(JSON.parse(JSON.stringify(entity)));
        } catch (error) {
            throw new Error('Falha ao atualizar ' + entity.constructor.name);
        }
    }
    public delete(id: string): void {
        try {
            this.firestoreCollection.doc(id).delete();
        } catch (error) {
            throw new Error('Falha ao excluir ' + this.constructor.name);
        }
    }
    private clean(entity: any) {
        for (const propName in entity) {
            if (entity[propName] === null || entity[propName] === undefined) {
                delete entity[propName]
            }
        }
    }
}
