import { FirestoreService } from './firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { AutenticacaoService } from './autenticacao.service';
import { Servico } from 'app/models/servico.model';
@Injectable({providedIn: 'root'})
export class ServicoService extends FirestoreService<Servico> {
    constructor(private firestore: AngularFirestore, private autenticacaoService: AutenticacaoService) {
        super(firestore, 'clientes/' + autenticacaoService.cliente.id + '/servicos');
    }
    public async salvar(servico: Servico): Promise<void> {
        try {
            if (servico.id == null) {
                return this.add(servico);
            } else {
                return this.update(servico);
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
