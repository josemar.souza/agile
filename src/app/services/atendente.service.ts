import { FirestoreService } from './firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Atendente } from 'app/models/atendente.model';
import { AutenticacaoService } from './autenticacao.service';
@Injectable({providedIn: 'root'})
export class AtendenteService extends FirestoreService<Atendente> {
    constructor(private firestore: AngularFirestore, private autenticacaoService: AutenticacaoService) {
        super(firestore, 'clientes/'+ autenticacaoService.cliente.id + '/atendentes');
    }
    public async salvar(atendente: Atendente): Promise<void> {
        try {
            //Persistir usuário se não existir
            //Criar usuario no authentication
            delete atendente.cliente;
            delete atendente.usuario;
            if (atendente.id == null) {
                return this.add(atendente);
            } else {
                return this.update(atendente);
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
