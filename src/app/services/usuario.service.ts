import { FirestoreService } from './firestore.service';
import { Usuario } from 'app/models/usuario.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Perfil } from 'app/models/perfil.enum';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UsuarioService extends FirestoreService<Usuario> {
    constructor(private firestore: AngularFirestore) {
        super(firestore, 'usuarios');
    }
    public async salvar(usuario: Usuario): Promise<any> {
        const _self = this;
        return await this.validar(usuario)
                    .then(async function() {
                        delete usuario.senha;
                        if (!usuario.id) {
                            return await _self.add(usuario).catch(function(error) {
                                throw error;
                            });
                        } else {
                            return await _self.update(usuario).catch(function(error) {
                                throw error;
                            });
                        }
                    });
    }
    public listarPorClientePerfil(clienteId: string, perfil: Perfil): Observable<Usuario[]> {
        try {
            return this.firestore.collection<Usuario>('usuarios', ref =>
                                            ref.where('perfis', 'array-contains', perfil)
                                            .where('clienteId', '==', clienteId)
                                            .orderBy('nome')
                                        ).valueChanges();
        } catch (error) {
            throw new Error('Falha ao listar ' + this.constructor.name);
        }
    }
    public async validar(usuario: Usuario): Promise<any> {
        //NOTE: Validando campos obrigarórios
        if (!usuario.clienteId) {
            throw Error('Cliente não informado').name = 'validation-error';
        }
        if (!usuario.email) {
            throw Error('E-mail não informado').name = 'validation-error';
        }
        if (!usuario.login) {
            throw Error('Login não informado').name = 'validation-error';
        }
        if (!usuario.nome) {
            throw Error('Nome não informado').name = 'validation-error';
        }
        if (!usuario.id) {
            //NOTE: Verifica se o  login já existe
            await this.firestore.collection<Usuario>('usuarios').ref.where('login', '==', usuario.login).get().then((col) => {
                if (col.docs.length > 0) {
                    throw Error('Já existe um usuário com o login ' + usuario.login + ' cadastrado');
                }
              }).catch(function(error) {
                error.name = 'validation-error';
                throw error;
              });
            //NOTE: Verifica se o e-mail já existe
            await this.firestore.collection<Usuario>('usuarios').ref.where('email', '==', usuario.email).get().then((col) => {
                if (col.docs.length > 0) {
                  throw new Error('Já existe um usuário com o e-mail ' + usuario.email + ' cadastrado');
                }
              }).catch(function(error) {
                error.name = 'validation-error';
                throw error;
              });
        }
    }
 }
