import { FirestoreService } from './firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Atendente } from 'app/models/atendente.model';
import { Cliente } from 'app/models/cliente.model';
@Injectable({providedIn: 'root'})
export class ClienteService extends FirestoreService<Cliente> {
    constructor(private firestore: AngularFirestore) {
        super(firestore, 'clientes');
    }
    public async salvar(cliente: Cliente): Promise<void> {
        try {
            if (cliente.id == null) {
                return this.add(cliente);
            } else {
                return this.update(cliente);
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
