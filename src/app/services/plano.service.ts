import { FirestoreService } from './firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Plano } from 'app/models/plano.model';
import { Observable } from 'rxjs';
@Injectable({providedIn: 'root'})
export class PlanoService extends FirestoreService<Plano> {
    constructor(private firestore: AngularFirestore) {
        super(firestore, 'planos');
    }
    public listarOrdenadoPorValor(): Observable<Plano[]> {
        try {
            return this.firestore.collection<Plano>('planos', ref =>
                                            ref.orderBy('valor')
                                        ).valueChanges();
        } catch (error) {
            throw new Error('Falha ao listar ' + this.constructor.name);
        }
    }
    public async salvar(plano: Plano): Promise<void> {
        try {
            if (plano.id == null) {
                return this.add(plano);
            } else {
                return this.update(plano);
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
