import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { AtendenteComponent } from 'app/pages/atendente/atendente.component';
import { ClienteComponent } from 'app/pages/cliente/cliente.component';
import { MaskDevmilDirective } from 'app/directives/mask-devmil.directive';
import { ServicoComponent } from 'app/pages/servico/servico.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    LoadingBarRouterModule,
    LoadingBarHttpClientModule,
  ],
  declarations: [
    DashboardComponent,
    AtendenteComponent,
    ClienteComponent,
    MaskDevmilDirective,
    ServicoComponent
  ]
})

export class AdminLayoutModule {}
