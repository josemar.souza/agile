import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { AtendenteComponent } from 'app/pages/atendente/atendente.component';
import { ClienteComponent } from 'app/pages/cliente/cliente.component';
import { ServicoComponent } from 'app/pages/servico/servico.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',        component: DashboardComponent },
    { path: 'atendente',        component: AtendenteComponent },
    { path: 'cliente',          component: ClienteComponent },
    { path: 'servico',          component: ServicoComponent }
];
