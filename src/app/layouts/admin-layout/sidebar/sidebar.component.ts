import { Component, OnInit } from '@angular/core';


export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: '/dashboard',   title: 'Dashboard',     icon: 'nc-sound-wave',         class: '' },
    { path: '/atendente',   title: 'Atendentes',    icon: 'nc-single-02',      class: '' },
    { path: '/cliente',    title: 'Clientes',      icon: 'nc-bank',      class: '' },
    { path: '/servico',    title: 'Servicos',      icon: 'nc-settings',      class: '' },
    { path: '/guiche',     title: 'Guiches',       icon: 'nc-shop',      class: '' },
    { path: '/painel',     title: 'Painéis',       icon: 'nc-tv-2',      class: '' },
    { path: '/faturamento', title: 'Faturamento',   icon: 'nc-paper',      class: '' },
    { path: '/upgrade',       title: 'Mudar de plano',      icon: 'nc-spaceship',    class: 'active-pro' },
];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
}
