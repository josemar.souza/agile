export class Avaliacao {
    constructor(
        public data: Date,
        public rating: number,
        public comentario: string
    ) { }
}