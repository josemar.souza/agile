import { Model } from "./model";
import { Chamada } from "./chamada";
import { TipoAtendimento } from "./tipo-atendimento.enum";
import { Servico } from "./servico.model";
import { SituacaoSenha } from "./situacao-senha.enum";

export class Senha extends Model {
    public chamadas: Array<Chamada>;
    constructor(
        public numero: string,
        public servico: Servico,
        public tipo: TipoAtendimento,
        public emissao: Date,
        public situacao: SituacaoSenha
    ){
        super();
    }
}