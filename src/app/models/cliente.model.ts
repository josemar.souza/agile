import { Model } from './model';
import { Plano } from './plano.model';
import { Contato } from './contato.model';
import { TipoCliente } from './tipo-cliente.enum';

export class Cliente extends Model {
    public contato: Contato;
    public id: string;
    constructor(
        public tipo: TipoCliente,
        public documento: string,
        public nome: string,
        public plano: Plano
    ) {
        super();
        this.contato = new Contato('', '', '');
    }
}
