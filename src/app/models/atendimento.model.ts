import { Model } from "./model";
import { Avaliacao } from './avaliacao';
import { Senha } from './senha.model';
import { Guiche } from './guiche.model';
import { Atendente } from './atendente.model';

export class Atendimento extends Model {
    public termino: Date;
    public comentario: string;
    public Avalicao: Avaliacao;
    constructor(
        public senha: Senha,
        public guiche: Guiche,
        public atendente: Atendente,
        public inicio: Date
    ){
        super();
    }
}