export enum Perfil {
    ADMINISTRADOR = 'Administrador',
    GESTOR = 'Gestor',
    ATENDENTE = 'Atendente'
}