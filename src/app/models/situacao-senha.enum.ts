export enum SituacaoSenha {
    EMITIDA = "Emitida",
    ATENDIMENTO = "Em atendimento",
    ATENDIDA = "Atendida",
    ABANDONO = "Abandono"
}