export enum TipoFaturamento {
    MENSAL =  'Mensal',
    TRIMESTRAL = 'Trimestral',
    ANUAL = 'Anual'
}