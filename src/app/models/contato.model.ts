export class Contato {
    constructor (
        public celular: string,
        public telefone: string,
        public email: string
    ) {}
}
