import { Model } from './model';

export class Servico extends Model {
    public icon: string;
    constructor(
        public nome: string,
        public descricao: string,
        public ativo: boolean
    ) {
        super();
    }
}
