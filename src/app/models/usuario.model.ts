import { Model } from './model';
import { Perfil } from './perfil.enum';

export class Usuario extends Model {
    public senha: string;
    public clienteId: string;
    public perfis: Array<Perfil>;
    public uid: string;
    constructor(
        public nome: string,
        public login: string,
        public email: string
    ) {
        super();
    }
}