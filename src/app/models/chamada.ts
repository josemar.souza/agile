import { Painel } from './painel.model';
import { Guiche } from './guiche.model';
import { Atendente } from './atendente.model';

export class Chamada {
    public chamadas: Array<Chamada>;
    constructor(
        public data: Date,
        public painel: Painel,
        public guiche: Guiche,
        public atendente: Atendente
    ) { }
}