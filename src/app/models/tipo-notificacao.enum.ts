export enum TipoNotificacao {
    INFO = 'Informação',
    WARNING = 'Alerta',
    ERROR = 'Erro',
    SUCCESS = 'Sucesso',
    DEFAULT = 'Padrão'
}
