export class Model {
    public id: string;
    public toPlainObject(): {} {
        return Object.assign({}, this);
    }
}
