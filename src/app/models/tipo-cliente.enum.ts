export enum TipoCliente {
    PESSOA_FISICA = 'Pessoa Física',
    PESSOA_JURIDICA = 'Pessoa Jurídica'
}
