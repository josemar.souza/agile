export enum SituacaoGuiche {
    LIVRE = "Livre",
    OCUPADO = "Em atendimento",
    INATIVO = "Inativo"
}