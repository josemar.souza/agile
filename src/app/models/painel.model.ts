import { Model } from "./model";
import { Guiche } from './guiche.model';

export class Painel extends Model {
    constructor(
        public identificacao: string,
        //public situacao: string,
        public guiches: Array<Guiche>
    ){
        super();
    }
}