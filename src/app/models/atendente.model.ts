import { Model } from './model';
import { Usuario } from './usuario.model';
import { Perfil } from './perfil.enum';
import { Cliente } from './cliente.model';

export class Atendente extends Model {
    public usuarioId: string;
    public clienteId: string;
    constructor(
        public cliente: Cliente,
        public usuario: Usuario
    ) {
        super();
        if (this.usuario.perfis == null) {
            this.usuario.perfis = new Array<Perfil>();
        }
        this.usuario.perfis.push(Perfil.ATENDENTE);
        this.clienteId = cliente.id;
        this.usuario.id = usuario.id;
    }
}
