import { Model } from './model';

export class Empresa extends Model {
    constructor(public cnpj: string,
                public nome: string,
                public nomeFantasia: string) {
        super();
    }
}
