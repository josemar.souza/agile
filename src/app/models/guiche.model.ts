import { Model } from "./model";
import { Servico } from "./servico.model";
import { SituacaoGuiche } from "./situacao-guiche.enum";

export class Guiche extends Model {
    constructor(
        public identificacao: string,
        public situacao: SituacaoGuiche,
        public servicos: Array<Servico>
    ){
        super();
    }
}