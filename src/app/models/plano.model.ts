import { TipoFaturamento } from './tipo-faturamento.enum';
import { Model } from './model';

export class Plano extends Model {
    public id: string;
    public vencimento: Date;
    constructor(
        public nome: string,
        public descricao: string,
        public tipoFaturamento: TipoFaturamento,
        public valor: number
    ) {
        super();
    }
}