import { Component } from '@angular/core';
import { ConnectionService } from 'ng-connection-service';
import { NotificationService } from './services/notification.service';
import { TipoNotificacao } from './models/tipo-notificacao.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  public isConnected = true;
  constructor(private connectionService: ConnectionService, private notification: NotificationService) {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (!isConnected) {
        this.notification.showNotification(TipoNotificacao.ERROR, 'Sem conexão');
      } else {
        this.notification.showNotification(TipoNotificacao.SUCCESS, 'Conectado novamente')
      }
    })
  }
}
